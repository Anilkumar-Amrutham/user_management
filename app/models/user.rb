class User < ApplicationRecord
	has_many :user_roles, dependent: :destroy
  has_many :roles, through: :user_roles
  has_many_attached :images
  has_many :user_meta_data
  accepts_nested_attributes_for :user_meta_data, allow_destroy: true
  validates :email, presence: true, uniqueness: {case_sensitive: false}
  accepts_nested_attributes_for :roles

  def user_meta_datum_record key
	   user_meta_data.detect { |ele| ele.meta_key == key }
	end

	def user_meta_datum(*args)
   meta_key, meta_value = args
   meta_key = meta_key && meta_key.to_s
   	if args.size == 1
      p = user_meta_datum_record(meta_key) && p.meta_value
   	elsif args.size == 2
      raise ArgumentError, "invalid key #{meta_key.inspect}" unless meta_key
      # Find or create a custom field object with the appropriate key
      p = user_meta_datum_record(meta_key) || self.user_meta_data.new(meta_key: meta_key)
      p.value = value
   	else raise ArgumentError, "wrong number of arguments (#{args.size} for 1 or 2)"
  	end
	end
end
