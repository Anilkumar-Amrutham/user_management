Rails.application.routes.draw do
	root :to => "home#index"
  resources :users do
  	collection do
	    get 'get_role_users'
	  end 
  end
  resources :roles do
  	collection do
	    get 'hide_roles'
	    post 'update_roles'
	    get 'get_role_users'
	  end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
